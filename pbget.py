#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright (C) 2012-2021  Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# TODO
# * Maybe add support for sparse SVN checkouts: https://www.archlinux.org/svn/

import argparse
import logging
import os
import platform
import sys
import urllib.error

import AUR.common
import AUR.RPC
import pm2ml
import XCGF
import XCPF

TTL = 5
SECONDS_PER_MINUTE = 60


def get_aur_pkgs(aur, pkgnames, maintainer=None):
    '''
    Search the AUR.
    '''
    if pkgnames:
        for pkg in AUR.RPC.insert_full_urls(aur.info(pkgnames)):
            yield pkg
    if maintainer:
        for pkg in AUR.RPC.insert_full_urls(aur.msearch(maintainer)):
            yield pkg


def get_upgradable(config=None, aur=None, aur_only=False):
    '''
    Determine upgradable AUR.RPC packages.
    '''
    args = ['-u']
    if config:
        args.extend(('-c', config))
    if aur is not None:
        args.append('--aur')
        if aur_only:
            args.append('--aur-only')

    pm2ml_pargs = pm2ml.parse_args(args)
    pm2ml_obj = pm2ml.Pm2ml(pm2ml_pargs)

    sync_pkgs, sync_deps, \
        aur_pkgs, aur_deps, _, _, _ = pm2ml_obj.resolve_targets_from_arguments()

    for pkg in sync_pkgs | sync_deps:
        yield pkg.name
    for pkg in aur_pkgs | aur_deps:
        yield pkg['Name']


parser = argparse.ArgumentParser(
    prog='pbget',
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='Retrieve PKGBUILDs and local source files from ABS and the AUR for makepkg.',
    #   epilog='''optional dependencies:
    # rsync                 Required for retrieving ABS files.
    # ''',
)
parser.add_argument(
    'pkgnames', metavar='<pkgname>', nargs='*',
    help="The source packages to retrieve."
)
parser.add_argument(
    '--arch', metavar='<architecture>',
    help='Set the desired package architecture.'
)
parser.add_argument(
    '--aur', action='store_true',
    help='Search the AUR. (requires python3-aur)'
)
parser.add_argument(
    '--aur-only', action='store_true',
    help='Only search the AUR. (requires python3-aur)'
)
parser.add_argument(
    '--clear', action='store_true',
    help='Clear the ABS Git cache.'
)
parser.add_argument(
    '--config', metavar='<path>', default='/etc/pacman.conf',
    help='Pacman configuration file. Default: %(default)s'
)
parser.add_argument(
    '--debug', action='store_true',
    help='Display debugging messages.'
)
parser.add_argument(
    '--dir', metavar='<path>', default='.',
    help='Set the output directory. Default: %(default)s'
)
parser.add_argument(
    '--maintainer', metavar='<maintainer>',
    help='Retrieve all AUR packages from the given maintainer. (implies --aur, requires python3-aur, may be extended to support official packages later)'
)
parser.add_argument(
    '--no-pull', dest='pull', action='store_false',
    help='Fetch Git sources instead of pulling them.'
)
parser.add_argument(
    '--trunk', action='store_true',
    help='Retrieve PKGBUILDs from the ABS trunk, which may included PKGBUILDs in testing or staging.'
)
parser.add_argument(
    '--resolve-pkgbases', action='store_true',
    help='Attempt to resolve package bases. This requires additional remote queries to the archlinux.org server and should only be used when necessary.'
)
parser.add_argument(
    '--testing', action='store_true',
    help='Search the testing branches of the ABS tree.'
)
parser.add_argument(
    '--ttl', type=int, default=TTL,
    help='The cache time-to-live, in minutes. Default: %(default)s'
)
parser.add_argument(
    '--upgradable', action='store_true',
    help='Search for all upgradable packages. (requires pyalpm and python3-aur)'
)


def main(args=None):  # pylint: disable=too-many-branches
    '''
    Parse the command-line arguments and retreive the requested PKGBUILDs.
    '''
    pargs = parser.parse_args(args)

    if pargs.debug:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    XCGF.configure_logging(level=log_level)

    did_something = False
    if pargs.clear:
        logging.info('clearing ABS Git cache')
        did_something = True
        XCPF.clear_abs_git_cache()

    if pargs.aur_only:
        pargs.aur = True

#   if pargs.testing:
#     pargs.abs = True

    if not pargs.arch:
        pargs.arch = platform.machine()

    if pargs.aur:
        aur = AUR.RPC.AurRpc(ttl=pargs.ttl * SECONDS_PER_MINUTE)
    else:
        aur = None

    output_dir = pargs.dir
    os.makedirs(output_dir, exist_ok=True)

    pkgnames = set(pargs.pkgnames)
    if pargs.upgradable:
        pkgnames |= set(get_upgradable(
            config=pargs.config,
            aur=aur,
            aur_only=pargs.aur_only
        ))

    if not pkgnames:
        # TODO
        # Remove this restriction when I have a scraper for
        #   https://www.archlinux.org/packages/?packager=
        if pargs.aur and pargs.maintainer:
            pargs.aur_only = True
        else:
            if not did_something:
                logging.info('nothing to do')
            return

    if not pargs.aur_only:
        opi = XCPF.OfficialPkgInfo(
            config=pargs.config,
            arch=pargs.arch,
            testing=pargs.testing,
            ttl=pargs.ttl * SECONDS_PER_MINUTE,
            log_level=logging.INFO,
            resolve_pkgbases=pargs.resolve_pkgbases,
        )

        if not pargs.aur_only:
            logging.info('searching ABS Git interface')
            for pkgname, url in opi.retrieve_abs_via_git(output_dir, pkgnames, pull=pargs.pull, trunk=pargs.trunk):
                logging.info('found %s at %s', pkgname, url)
                pkgnames.remove(pkgname)

    if pargs.aur and (pkgnames or pargs.maintainer):
        logging.info('searching AUR')
        aurpkgs = list(get_aur_pkgs(aur, pkgnames, maintainer=pargs.maintainer))
        names = set(p['Name'] for p in aurpkgs)
        pkgnames -= (names | set(n.lower() for n in names))
        for pkg in AUR.RPC.download_git_repo(output_dir, aurpkgs, pull=pargs.pull):
            logging.info(
                'found %s at %s',
                pkg['Name'],
                AUR.common.AUR_GIT_URL_FORMAT.format(pkg['PackageBase'])
            )
        # The remaining pkgnames may be pkgbases. The RPC info does not find these.
        pseudo_aurpkgs = list({'PackageBase': pkgname} for pkgname in pkgnames)
        for pkg in AUR.RPC.download_git_repo(output_dir, pseudo_aurpkgs, warn=True):
            logging.info(
                'found %s in AUR (PackageBase: %s)',
                ', '.join(sorted(pkg['Names'])),
                pkg['PackageBase']
            )
            pkgnames.remove(pkg['PackageBase'])

    if pkgnames:
        for pkgname in pkgnames:
            logging.warning('%s not found', pkgname)


def run_main(args=None):
    '''
    Wrapper around main to handle exceptions.
    '''
    try:
        main(args=args)
    except KeyboardInterrupt:
        pass
    except (OSError, urllib.error.HTTPError, AUR.RPC.AurError) as err:
        return str(err)
    return None


if __name__ == '__main__':
    sys.exit(run_main())
