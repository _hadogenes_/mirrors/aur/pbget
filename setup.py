#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='pbget',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1640341437)),
  description='''Retrieve PKGBUILDs and local source files from Git, ABS and the AUR for makepkg.''',
  author='Xyne',
  author_email='gro xunilhcra enyx, backwards',
  url='''http://xyne.dev/projects/pbget''',
  py_modules=['pbget'],
  scripts=['pbget']
)
